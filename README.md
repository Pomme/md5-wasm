# How to compile

Requires Go 1.11

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

# Launch server

Using gexec : https://github.com/shurcooL/goexec

```bash
goexec 'http.ListenAndServe(":8081", http.FileServer(http.Dir(".")))'
```

# What's next

## Benchmarks

From https://github.com/cvdfoundation/open-images-dataset#download-images-with-bounding-boxes-annotations

```bash
wget $(cat ../open-images-dataset-train0.tsv| head -n 200 | tail -n 199 | awk '{print $1}')
```

## Reducing main.wasm size

Actual file is 2.5Mo but it could be a lot less

https://github.com/golang/go/wiki/WebAssembly#reducing-the-size-of-wasm-files

## Trying out Rust

I expect performance to be the same. Just would like to test the toolchain.

https://hacks.mozilla.org/2018/12/rust-2018-is-here

# Useful links

https://github.com/golang/go/wiki/WebAssembly

https://tutorialedge.net/golang/go-webassembly-tutorial/

https://golang.org/pkg/syscall/js/