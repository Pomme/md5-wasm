package main

import "fmt"
import "crypto/md5"
import "syscall/js"

func wasm_md5(strToHash []js.Value) {
	data := []byte(js.Value.String(strToHash[0]))
	fmt.Printf("%x\n", md5.Sum(data));
}

func registerCallbacks() {
	js.Global().Set("wasm_md5", js.NewCallback(wasm_md5))
}

func main() {
	c := make(chan struct{}, 0)

	println("WASM Go Initialized")
	// register functions
	registerCallbacks()
	<-c
}
